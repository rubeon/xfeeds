# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xfeeds', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='xfeed',
            name='cloud',
            field=models.ForeignKey(to='xfeeds.Cloud', blank=True),
            preserve_default=True,
        ),
    ]
