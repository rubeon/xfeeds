# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xfeeds', '0003_auto_20141216_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='read_flag',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='channel',
            name='link',
            field=models.CharField(unique=True, max_length=1024),
            preserve_default=True,
        ),
    ]
