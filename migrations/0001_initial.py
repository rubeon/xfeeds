# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cloud',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('domain', models.CharField(max_length=256)),
                ('port', models.IntegerField(default=80)),
                ('path', models.CharField(max_length=100, blank=True)),
                ('register_procedure', models.CharField(max_length=100, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='XFeed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('link', models.CharField(max_length=1024)),
                ('description', models.TextField()),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
                ('updated', models.DateTimeField(default=datetime.datetime.now)),
                ('language', models.CharField(max_length=100, blank=True)),
                ('copyright', models.CharField(max_length=1024, blank=True)),
                ('managing_editor', models.EmailField(max_length=75, blank=True)),
                ('web_master', models.EmailField(max_length=75, blank=True)),
                ('pub_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('last_build_date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('generator', models.CharField(max_length=1024, blank=True)),
                ('cloud', models.ForeignKey(to='xfeeds.Cloud')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
