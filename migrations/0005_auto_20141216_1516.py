# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xfeeds', '0004_auto_20141216_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='cloud',
            field=models.ForeignKey(blank=True, to='xfeeds.Cloud', null=True),
            preserve_default=True,
        ),
    ]
