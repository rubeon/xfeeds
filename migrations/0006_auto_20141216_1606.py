# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xfeeds', '0005_auto_20141216_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='category',
            field=models.ManyToManyField(to='xfeeds.Category', null=True, blank=True),
            preserve_default=True,
        ),
    ]
