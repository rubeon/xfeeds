# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('xfeeds', '0002_auto_20141216_1109'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('domain', models.URLField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=256)),
                ('link', models.URLField()),
                ('description', models.TextField(blank=True)),
                ('author', models.EmailField(max_length=75, blank=True)),
                ('category', models.ManyToManyField(to='xfeeds.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameModel(
            old_name='XFeed',
            new_name='Channel',
        ),
        migrations.AddField(
            model_name='item',
            name='channel',
            field=models.ForeignKey(to='xfeeds.Channel'),
            preserve_default=True,
        ),
    ]
