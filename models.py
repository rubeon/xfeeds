from django.db import models
import datetime
import logging

import xfeeds.utils

logger = logging.getLogger(__name__)

# Create your models here.

class Channel(models.Model):
    """ 
    Model for feeds 
    RSS Spec is here:
    http://cyber.law.harvard.edu/rss/rss.html
    """
    # required fields
    title = models.CharField(blank=False, max_length=256)
    link = models.CharField(blank=False, max_length=1024, unique=True)
    description = models.TextField(blank=False)
    created = models.DateTimeField(blank=False, default=datetime.datetime.now)
    updated = models.DateTimeField(blank=False, default=datetime.datetime.now)

    # optional fields
    language = models.CharField(blank=True, max_length=100)
    copyright = models.CharField(blank=True, max_length=1024)
    managing_editor = models.EmailField(blank=True)
    web_master = models.EmailField(blank=True)
    pub_date = models.DateTimeField(blank=True, default=datetime.datetime.now)
    last_build_date = models.DateTimeField(blank=True, default=datetime.datetime.now)
    generator = models.CharField(blank=True, max_length=1024)
    
    cloud = models.ForeignKey('Cloud', blank=True, null=True)
    
    def __unicode__(self):
        return self.title
    
    
    
class Cloud(models.Model):
    """ pub/sub cloud (see rss 2.0 spec) """
    domain = models.CharField(blank=False, max_length=256)
    port = models.IntegerField(blank=False, null=False, default=80)
    path = models.CharField(blank=True, max_length=100)
    # FIXME: need to check how to structure register_procedure
    register_procedure = models.CharField(blank=True, max_length=100)
    # skip_hours
    # skip_days

class Item(models.Model):
    """
    These are the items in a channel 
    feedparser calls them 'entries', but the spec says 'item', so...
    """
    
    channel = models.ForeignKey('Channel')
    
    title = models.CharField(blank=False, max_length=256)
    link = models.URLField(blank=False)
    description = models.TextField(blank=True)
    author = models.EmailField(blank=True)
    category = models.ManyToManyField('Category', blank=True, null=True)
    read_flag = models.BooleanField(default=False)
    
class Category(models.Model):
    """(Category description)"""
    title = models.CharField(blank=False, max_length=100)
    domain = models.URLField(blank=True)
    

def channels_from_url(url):
    """ 
    takes a URL, and creates channels from it
    """
    logger.debug("channels_from_url entered")
    title, feeds = xfeeds.utils.feeds_from_page(url)
    logger.debug(title)
    logger.debug(feeds)
    channel_list = []
    for feed in feeds:
         res = xfeeds.utils.feed_to_channel(title, feed)
         if 'comment' not in res['link'].lower(): # hack FIXME
             channel_list.append(res)
    logger.info(res)
    return channel_list

def update_channel(channel):
    """
    Adds items for the channel
    """
    url = channel.link
    res = []
    for item in xfeeds.utils.items_from_feed(url):
        new_item={}
        new_item['channel'] = channel
        new_item['title'] = item['title']
        new_item['link'] = item['link']
        new_item['description'] = item.get('content') or item.get('description')
        
        i = Item(channel=channel, title=item['title'], link=item['link'], description=item.get('content') or item.get('description'))
        print i
        i.save()
        res.append(i)
    return res