from django.contrib import admin
from .models import Channel, Cloud, Item
# Register your models here.

@admin.register(Channel)
class AdminXFeed(admin.ModelAdmin):
    """
    Admin class for XFeed
    """
    list_display=('title','link', 'updated')
    
@admin.register(Cloud)
class AdminCloud(admin.ModelAdmin):
    """
    Admin class for Cloud objects
    """
    pass


@admin.register(Item)
class AdminItem(admin.ModelAdmin):
    """
    Admin class for Cloud objects
    """
    list_display=('title', 'link', 'channel', 'read_flag')