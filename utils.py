import feedparser
from BeautifulSoup import BeautifulSoup as bs
import urllib
import logging
import datetime
import urlparse
# from models import Cloud, Item, Category, Channel

logger = logging.getLogger(__name__)

def feed_to_channel(title, feed):
    # takes a feed, and turns it into a channel object...
    logger.info("entering feed_to_channel")
    logger.debug(feed)
    channel = {}
    channel['title'] = title
    channel['link'] = feed.get('href')
    # channel['created'] = datetime.datetime.now()
    # channel['updated'] = datetime.datetime.now()
    return channel
    
def feeds_from_page(url):
    """
    takes a URL (str), and creates feeds from it...
    """
    logger.info("feeds_from_page entered")
    page = urllib.urlopen(url).read()
    logger.info("Got %d bytes" % len(page))
    
    # create html parser
    soup = bs(page)
    title = soup.title.string
    # find feeds...
    feed_list = find_feeds(soup)
    logger.info("found %d feeds" % len(feed_list))
    # convert relative to absolute links
    print feed_list
    for feed in feed_list:
        href = feed.get('href')
        if url not in href:
            feed['href'] = urlparse.urljoin(url, href)
    return title, feed_list
    
def parse_feed(feed):
    """ parses feed content based at arg `feed`"""
    logger.info("parse_feed entered")
    logger.debug("%d bytes" % len(feed))
    rawdata = feed
    d = feedparser.parse(rawdata)
    logger.info("Feed parsed, returning")
    return d

def find_feeds(soup):
    # detects the feed in a URL
    # pass the entire HTML text as page
    # returns a list of feed URLs
    logger.info("find_feed entered")
    #   print soup
    res = []
    for link in soup.findAll('link'):
        logger.info("Pressing %s" % link)
        if str(link.get('type')).lower() == "application/rss+xml":
            res.append(link)
        elif str(link.get('type')).lower() == "application/atom+xml":
            res.append(link)
    
    logger.info("Found feeds: %s" % str(res))
    return res

def items_from_feed(feed):
    """
    feed is a URL, as stored in Channel.
    Sheesh, the terminology :-/
    
    returns a list of dictionaries for turning into 
    xfeeds.models.Item
    
    """
    myfeed = parse_feed(feed)
    items = []
    for entry in myfeed.entries:
        items.append(entry)
    return items

if __name__ == '__main__':
    import sys, pprint, os
    print("testing with %s" % sys.argv[-1])
    page = "http://www.daringfireball.net"
    
    
    title, feeds = feeds_from_page(page)
    print title
    print feeds
    for feed in feeds:
        print feed_to_channel(title, feed)
        print feed['href']
        items_from_feed(feed['href'])