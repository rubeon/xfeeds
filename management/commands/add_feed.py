from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from xfeeds.models import Channel, channels_from_url
import xfeeds.utils
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Adds a feed from a URL'
    args = 'url'
    option_list = BaseCommand.option_list + (
        make_option('--url', dest='url', help='URL of page to search for feeds'),
    )

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_id', nargs='+', type=int)
    # 
    def handle(self, *args, **options):
        logger.info("handle entered")
        
        # parse the feeds?
        url = options['url']
        logger.debug(options)
        logger.info("Processing %s" % url)
        feed_list = channels_from_url(url)
        print __name__, feed_list
        logger.info(feed_list)
        # try to create a channel with this...
        for channel in feed_list:
            logger.info("Trying to create channel: %s" % channel)
            # c = Channel(title=channel['title'], link=channel['link'])
            c = Channel(title=channel['title'], link=channel['link'])
            c.save()
            print __name__, c
        return
        
        for poll_id in options['poll_id']:
            try:
                poll = Poll.objects.get(pk=poll_id)
            except Poll.DoesNotExist:
                raise CommandError('Poll "%s" does not exist' % poll_id)

            poll.opened = False
            poll.save()

            self.stdout.write('Successfully closed poll "%s"' % poll_id)
