from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from xfeeds.models import Channel, update_channel
import xfeeds.utils
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Gathers items from a channel'
    args = 'channel-id'
    option_list = BaseCommand.option_list + (
        make_option('--channel-id', dest='channel_id', help='Channel ID to update'),
    )

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_id', nargs='+', type=int)
    # 
    def handle(self, *args, **options):
        logger.info("handle entered")
        c = Channel.objects.get(pk=int(options['channel_id']))
        res = update_channel(c)
        print __name__, res
        
        